package com.gtech.ServicesImpl;

import com.gtech.Dao.Services.JournalAccountLogDaoServices;
import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;
import com.gtech.Services.JournalAccountLogServices;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class JournalAccountLogServicesTest {

    @InjectMocks
    JournalAccountLogServices journalAccountLogServices = new JournalAccountLogServicesImpl();

    @Mock
    JournalAccountLogDaoServices journalAccountLogDaoServices;

    @Test
    public void submit_Success() throws ParseException {

        JournalAccountLog accountLog = new JournalAccountLog();

        Mockito.when(journalAccountLogDaoServices.submit(accountLog)).thenReturn(accountLog);

        journalAccountLogServices.submit(accountLog);
    }

    @Test
    public void bulSubmit_Success() throws ParseException {

        List<JournalAccountLog> accountLog = new ArrayList<>();

        Mockito.when(journalAccountLogDaoServices.bulkSubmit(accountLog)).thenReturn(accountLog);

        journalAccountLogServices.bulkSubmit(accountLog);
    }
}
