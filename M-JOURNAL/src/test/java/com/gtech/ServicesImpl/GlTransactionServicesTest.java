package com.gtech.ServicesImpl;

import static org.assertj.core.api.Assertions.assertThat;

import com.gtech.Dao.Services.GlBranchTrxDaoServices;
import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Entity.GlBranchTrxResponse;
import com.gtech.GlTransaction.Persistance.Entity.GlBranchTransaction;
import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;
import com.gtech.GlTransaction.Persistance.Repository.GlBranchTrxRepository;
import com.gtech.Services.GlBranchTrxServices;
import com.gtech.Services.GlTransactionServices;
import com.gtech.Services.JournalAccountLogServices;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
public class GlTransactionServicesTest {

    @InjectMocks
    GlTransactionServices glTransactionServices = new GlTransactionServicesImpl();

    @Mock
    GlBranchTrxServices glBranchTrxServices;

    @Mock
    GlBranchTrxDaoServices glBranchTrxDaoServices;

    @Mock
    GlBranchTrxRepository glBranchTrxRepository;

    @Mock
    JournalAccountLogServices journalAccountLogServices;


    @Test
    public void glTransactionSubmit_Success() throws ParseException {
        GlBranchTrxRequest glBranchTrxRequest = new GlBranchTrxRequest();
        GlBranchTrxResponse glBranchTrxResponse =  new GlBranchTrxResponse();
        GlBranchTransaction glBranchTransaction = new GlBranchTransaction();
        JournalAccountLog accountLog = new JournalAccountLog();
        List<JournalAccountLog> logList = new ArrayList<>();

        glBranchTrxRequest.setTransactionDate(new Date());
        glBranchTrxRequest.setReffNo("reffNo");
        glBranchTrxRequest.setDebitAccount("debitAcc");
        glBranchTrxRequest.setCreditAccount("creditAcc");

        glBranchTrxResponse.setTransactionDate(new Date());
        glBranchTrxResponse.setReffNo("reffNo");
        glBranchTrxResponse.setDebitAccount("debitAcc");
        glBranchTrxResponse .setCreditAccount("creditAcc");

        glBranchTransaction.setTransactionDate(new Date());

        accountLog.setAmount(new BigDecimal(0));
        logList.add(accountLog);


        Mockito.when(glBranchTrxRepository.save(glBranchTransaction)).thenReturn(glBranchTransaction);
        Mockito.when(glBranchTrxDaoServices.submit(glBranchTransaction)).thenReturn(glBranchTransaction);
        Mockito.when(glBranchTrxServices.submit(glBranchTrxRequest)).thenReturn(glBranchTrxResponse);
        journalAccountLogServices.bulkSubmit(logList);

        GlBranchTrxResponse result =  glTransactionServices.submitProcessGlTransaction(glBranchTrxRequest);
        assertThat(result).isEqualTo(glBranchTrxResponse);
    }
}
