package com.gtech.ServicesImpl;

import com.gtech.Dao.Services.GlBranchTrxDaoServices;
import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Persistance.Entity.GlBranchTransaction;
import com.gtech.Services.GlBranchTrxServices;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;

@RunWith(SpringRunner.class)
public class GlBranchTrxServicesTest {

    @InjectMocks
    GlBranchTrxServices glBranchTrxServices = new GlBranchTrxServicesImpl();

    @Mock
    GlBranchTrxDaoServices glBranchTrxDaoServices;


    @Test
    public void glBranchTransactionSubmit_Success() throws ParseException {

        GlBranchTransaction glBranchTransaction = new GlBranchTransaction();
        GlBranchTrxRequest trxRequest = new GlBranchTrxRequest();

        glBranchTransaction.setCreditAccount("creditAccount");
        glBranchTransaction.setCreditAmount(new BigDecimal(10000));
        glBranchTransaction.setDebitAccount("debitAccount");
        glBranchTransaction.setDebitAmount(new BigDecimal(10000));

        trxRequest.setCreditAccount("creditAccount");
        trxRequest.setCreditAmount(new BigDecimal(10000));
        trxRequest.setDebitAccount("debitAccount");
        trxRequest.setDebitAmount(new BigDecimal(10000));

        Mockito.when(glBranchTrxDaoServices.submit(glBranchTransaction)).thenReturn(glBranchTransaction);

        glBranchTrxServices.submit(trxRequest);
    }
}
