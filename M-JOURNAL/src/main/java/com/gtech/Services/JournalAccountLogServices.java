package com.gtech.Services;

import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;

import java.util.List;

public interface JournalAccountLogServices {
    JournalAccountLog mapperFromGlTrxRequest(GlBranchTrxRequest trxRequest);
    void submit (JournalAccountLog journalAccountLog);
    void bulkSubmit (List<JournalAccountLog> accountLogList);
}
