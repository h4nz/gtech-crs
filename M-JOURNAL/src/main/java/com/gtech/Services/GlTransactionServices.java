package com.gtech.Services;

import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Entity.GlBranchTrxResponse;

import java.text.ParseException;

public interface GlTransactionServices {
    GlBranchTrxResponse submitProcessGlTransaction(GlBranchTrxRequest glBranchTrxRequest) throws ParseException;
}
