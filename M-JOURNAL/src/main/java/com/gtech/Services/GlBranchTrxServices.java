package com.gtech.Services;

import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Entity.GlBranchTrxResponse;

import java.text.ParseException;

public interface GlBranchTrxServices {
    GlBranchTrxResponse submit (GlBranchTrxRequest trxRequest) throws ParseException;
}
