package com.gtech.ServicesImpl;

import com.gtech.Common.TransactionType;
import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Entity.GlBranchTrxResponse;
import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;
import com.gtech.Services.GlBranchTrxServices;
import com.gtech.Services.GlTransactionServices;
import com.gtech.Services.JournalAccountLogServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class GlTransactionServicesImpl implements GlTransactionServices {

    @Autowired
    GlBranchTrxServices glBranchTrxServices;

    @Autowired
    private JournalAccountLogServices journalAccountLogServices;

    @Override
    @Transactional
    public GlBranchTrxResponse submitProcessGlTransaction(GlBranchTrxRequest glBranchTrxRequest) throws ParseException {
        GlBranchTrxResponse glBranchTrxResponse = this.glBranchTrxServices.submit(glBranchTrxRequest);
        journalAccountLogServices.bulkSubmit(mapperAccountLogList(glBranchTrxRequest));
        return glBranchTrxResponse;
    }

    private List<JournalAccountLog> mapperAccountLogList (GlBranchTrxRequest glRequest){
        JournalAccountLog debitAccount = new JournalAccountLog();
        JournalAccountLog creditAccount = new JournalAccountLog();
        List<JournalAccountLog> journalAccountLogs = new ArrayList<>();


        if(glRequest.getDebitAccount() != null && glRequest.getDebitAccount().length() > 0){
            debitAccount.setTransactionDate(glRequest.getTransactionDate());
            debitAccount.setAccountNo(glRequest.getDebitAccount());
            debitAccount.setReffNo(glRequest.getReffNo());
            debitAccount.setAmount(glRequest.getDebitAmount());
            debitAccount.setJournalType(TransactionType.D.label);
        }

        if(glRequest.getCreditAccount() != null && glRequest.getCreditAccount().length() > 0){
            creditAccount.setTransactionDate(glRequest.getTransactionDate());
            creditAccount.setAccountNo(glRequest.getCreditAccount());
            creditAccount.setReffNo(glRequest.getReffNo());
            creditAccount.setAmount(glRequest.getCreditAmount());
            creditAccount.setJournalType(TransactionType.C.label);
        }

        journalAccountLogs.add(debitAccount);
        journalAccountLogs.add(creditAccount);
        return journalAccountLogs;
    }

}
