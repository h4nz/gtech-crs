package com.gtech.ServicesImpl;

import com.gtech.Dao.Services.JournalAccountLogDaoServices;
import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;
import com.gtech.Services.JournalAccountLogServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JournalAccountLogServicesImpl implements JournalAccountLogServices {

    @Autowired
    private JournalAccountLogDaoServices journalAccountLogDaoServices;

    @Override
    public JournalAccountLog mapperFromGlTrxRequest(GlBranchTrxRequest trxRequest) {
        return null;
    }

    @Override
    public void submit(JournalAccountLog journalAccountLog) {
        journalAccountLogDaoServices.submit(journalAccountLog);
    }

    @Override
    public void bulkSubmit(List<JournalAccountLog> accountLogList) {
        journalAccountLogDaoServices.bulkSubmit(accountLogList);
    }


}
