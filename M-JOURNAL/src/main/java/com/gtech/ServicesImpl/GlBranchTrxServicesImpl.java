package com.gtech.ServicesImpl;

import com.gtech.Dao.Services.GlBranchTrxDaoServices;
import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Entity.GlBranchTrxResponse;
import com.gtech.GlTransaction.Persistance.Entity.GlBranchTransaction;
import com.gtech.Services.GlBranchTrxServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class GlBranchTrxServicesImpl implements GlBranchTrxServices {

    @Autowired
    private GlBranchTrxDaoServices glBranchTrxDaoServices;

    private ModelMapper modelMapper = new ModelMapper();

    @Override
    public GlBranchTrxResponse submit(GlBranchTrxRequest trxRequest) throws ParseException {
        GlBranchTransaction glBranchTransaction = glBranchTrxDaoServices.submit(convertToEntity(trxRequest));
        return convertFromEntity(glBranchTransaction);
    }

    private GlBranchTransaction convertToEntity(Object object ) throws ParseException {
        GlBranchTransaction glBranchTransaction = modelMapper.map(object, GlBranchTransaction.class);
        return glBranchTransaction;
    }

    private GlBranchTrxResponse convertFromEntity(GlBranchTransaction glBranchTransaction){
        GlBranchTrxResponse glBranchTrxResponse = modelMapper.map(glBranchTransaction,GlBranchTrxResponse.class);
        return glBranchTrxResponse;
    }


}
