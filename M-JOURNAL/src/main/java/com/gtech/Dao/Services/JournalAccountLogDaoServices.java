package com.gtech.Dao.Services;


import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;

import java.util.List;

public interface JournalAccountLogDaoServices {
    JournalAccountLog submit (JournalAccountLog journalAccountLog);
    List<JournalAccountLog> bulkSubmit (List<JournalAccountLog> journalAccountLogList);
}
