package com.gtech.Dao.Services;

import com.gtech.GlTransaction.Persistance.Entity.GlBranchTransaction;

public interface GlBranchTrxDaoServices {
    GlBranchTransaction submit (GlBranchTransaction glBranchTransaction);
}
