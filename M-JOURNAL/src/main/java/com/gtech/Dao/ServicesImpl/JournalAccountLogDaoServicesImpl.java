package com.gtech.Dao.ServicesImpl;

import com.gtech.Dao.Services.JournalAccountLogDaoServices;
import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;
import com.gtech.GlTransaction.Persistance.Repository.JournalAccountLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JournalAccountLogDaoServicesImpl implements JournalAccountLogDaoServices {

    @Autowired
    private JournalAccountLogRepository journalAccountLogRepository;

    @Override
    public JournalAccountLog submit(JournalAccountLog journalAccountLog) {
        return journalAccountLogRepository.save(journalAccountLog);
    }

    @Override
    public List<JournalAccountLog> bulkSubmit(List<JournalAccountLog> journalAccountLogList) {
        return journalAccountLogRepository.saveAll(journalAccountLogList);
    }
}
