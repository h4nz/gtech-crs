package com.gtech.Dao.ServicesImpl;

import com.gtech.Dao.Services.GlBranchTrxDaoServices;
import com.gtech.GlTransaction.Persistance.Entity.GlBranchTransaction;
import com.gtech.GlTransaction.Persistance.Repository.GlBranchTrxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GlBranchTrxDaoServicesImpl implements GlBranchTrxDaoServices {

    @Autowired
    GlBranchTrxRepository glBranchTrxRepository;

    @Override
    public GlBranchTransaction submit(GlBranchTransaction glBranchTransaction) {
        glBranchTrxRepository.save(glBranchTransaction);
        return glBranchTransaction;
    }
}
