package com.gtech.Common;

public enum TransactionType {
    D("DEBET"),
    C("CREDIT");

    public final String label;

    private TransactionType(String label) {
        this.label = label;
    }
}
