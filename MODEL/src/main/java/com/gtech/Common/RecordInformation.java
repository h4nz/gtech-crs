package com.gtech.Common;

import lombok.Data;

import java.util.Date;

@Data
public class RecordInformation {
    private String createdBy;
    private Date createdDate;
    private String modifyBy;
    private Date modifyDate;
}
