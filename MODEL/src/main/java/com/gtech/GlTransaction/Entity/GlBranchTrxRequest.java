package com.gtech.GlTransaction.Entity;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class GlBranchTrxRequest {
    @NotEmpty
    @NotNull
    private Date transactionDate;
    @NotEmpty
    @NotNull
    private String reffNo;
    @NotEmpty
    @NotNull
    private String debitAccount;
    @NotEmpty
    @NotNull
    private String creditAccount;
    @NotEmpty
    @NotNull
    private BigDecimal debitAmount;
    @NotEmpty
    @NotNull
    private BigDecimal creditAmount;
}
