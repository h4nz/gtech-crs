package com.gtech.GlTransaction.Entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class GlBranchTrxResponse {
    private String id;
    private Date transactionDate;
    private String reffNo;
    private String debitAccount;
    private String creditAccount;
    private BigDecimal debitAmount;
    private BigDecimal creditAmount;
}
