package com.gtech.GlTransaction.Persistance.Repository;

import com.gtech.GlTransaction.Persistance.Entity.JournalAccountLog;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JournalAccountLogRepository extends MongoRepository<JournalAccountLog,String> {

}
