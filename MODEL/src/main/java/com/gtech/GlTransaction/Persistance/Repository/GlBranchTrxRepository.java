package com.gtech.GlTransaction.Persistance.Repository;

import com.gtech.GlTransaction.Persistance.Entity.GlBranchTransaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlBranchTrxRepository extends MongoRepository<GlBranchTransaction,String> {

}
