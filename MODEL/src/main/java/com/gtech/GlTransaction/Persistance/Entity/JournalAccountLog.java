package com.gtech.GlTransaction.Persistance.Entity;

import com.gtech.Common.RecordInformation;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Document(collection = "JOURNAL_ACCOUNT_LOG")
public class JournalAccountLog extends RecordInformation {
    @Id
    private String id;
    private String glBranchTrxId;
    private Date transactionDate;
    private String accountNo;
    private String description;
    private String reffNo;
    private String journalType;
    private BigDecimal Amount;
}
