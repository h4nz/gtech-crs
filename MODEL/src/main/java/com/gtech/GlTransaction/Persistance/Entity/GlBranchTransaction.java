package com.gtech.GlTransaction.Persistance.Entity;

import com.gtech.Common.RecordInformation;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Document(collection = "GL_BRANCH_TRANSACTION")
public class GlBranchTransaction extends RecordInformation {

    @Id
    private String id;
    private Date transactionDate;
    private String reffNo;
    private String debitAccount;
    private String creditAccount;
    private BigDecimal debitAmount;
    private BigDecimal creditAmount;
}
