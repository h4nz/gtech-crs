package com.gtech.MasterData.Repository;

import com.gtech.MasterData.Entity.UmUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UmUserRepository extends MongoRepository<UmUser,String> {

}
