package com.gtech.MasterData.Entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "UM_USER")
public class UmUser {

    @Id
    private String id;
    private String storeName;
    private String email;
    private String password;
}
