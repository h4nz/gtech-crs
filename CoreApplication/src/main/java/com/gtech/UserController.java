package com.gtech;

import com.gtech.MasterData.Entity.UmUser;
import com.gtech.MasterData.Repository.UmUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    UmUserRepository userRepository;


    @RequestMapping(value = "/user",method = RequestMethod.POST)
    public ResponseEntity<UmUser> userRegistration(@RequestBody UmUser request) throws Exception {
        try{
            this.userRepository.save(request);
            return ResponseEntity.ok().body(request);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
}
