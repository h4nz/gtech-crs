package com.gtech.GlBranchTrxController;

import com.gtech.GlTransaction.Entity.GlBranchTrxRequest;
import com.gtech.GlTransaction.Entity.GlBranchTrxResponse;
import com.gtech.Services.GlBranchTrxServices;
import com.gtech.Services.GlTransactionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/glBranchTransaction")
public class GlBranchTrxController {

    @Autowired
    GlBranchTrxServices glBranchTrxServices;

    @Autowired
    GlTransactionServices glTransactionServices;

    @RequestMapping(value = "/submit",method = RequestMethod.POST)
    public ResponseEntity<GlBranchTrxResponse> glBranchTrxSubmt(@RequestBody GlBranchTrxRequest trxRequest) throws Exception {
        try{
            return ResponseEntity.ok().body(this.glTransactionServices.submitProcessGlTransaction(trxRequest));
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
}
